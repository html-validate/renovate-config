# @html-validate/renovate-config changelog

### [2.0.2](https://gitlab.com/html-validate/renovate-config/compare/v2.0.1...v2.0.2) (2021-08-22)

### Bug Fixes

- update token once again ([ce998c5](https://gitlab.com/html-validate/renovate-config/commit/ce998c58a1dc40a61fa99d076d4796bc317fd817))

### [2.0.1](https://gitlab.com/html-validate/renovate-config/compare/v2.0.0...v2.0.1) (2021-08-18)

### Bug Fixes

- update github token ([243373f](https://gitlab.com/html-validate/renovate-config/commit/243373f516303854294daf0f4d593069b0e2ac57))

## [2.0.0](https://gitlab.com/html-validate/renovate-config/compare/v1.3.6...v2.0.0) (2021-06-27)

### ⚠ BREAKING CHANGES

- require NodeJS 12

### Features

- require NodeJS 12 ([8871c31](https://gitlab.com/html-validate/renovate-config/commit/8871c318cb112c3f71c0af0b6fae659e83681a78))

### [1.3.6](https://gitlab.com/html-validate/renovate-config/compare/v1.3.5...v1.3.6) (2021-02-28)

### Bug Fixes

- groupSlug for eslint monorepo ([d6c9c02](https://gitlab.com/html-validate/renovate-config/commit/d6c9c02db5866b404c30be3e766508f459fa2a78))

### [1.3.5](https://gitlab.com/html-validate/renovate-config/compare/v1.3.4...v1.3.5) (2021-02-28)

### Bug Fixes

- group `@html-validate/eslint-config` packages together ([3c33055](https://gitlab.com/html-validate/renovate-config/commit/3c33055d810b52971b7c3b1e85c49ee453fc4ad4))

### [1.3.4](https://gitlab.com/html-validate/renovate-config/compare/v1.3.3...v1.3.4) (2021-01-15)

### Bug Fixes

- all html-validate packages should have 0 stability days ([feec693](https://gitlab.com/html-validate/renovate-config/commit/feec6932e7bc62810d25721bac703433013c009e))

### [1.3.3](https://gitlab.com/html-validate/renovate-config/compare/v1.3.2...v1.3.3) (2020-12-19)

### Bug Fixes

- disable dashboard again ([a748b44](https://gitlab.com/html-validate/renovate-config/commit/a748b4453bb961448194852e2b65eb8db6b18433))

### [1.3.2](https://gitlab.com/html-validate/renovate-config/compare/v1.3.1...v1.3.2) (2020-12-17)

### Bug Fixes

- move github token to a separate config ([194eaa1](https://gitlab.com/html-validate/renovate-config/commit/194eaa1d29f52d5cb84aaafccf9e5c7163bd8a5b))

### Reverts

- Revert "fix: disable dependency dashboard" ([ae49a13](https://gitlab.com/html-validate/renovate-config/commit/ae49a13b1647a6aab13c3d0601fa170fc64e2deb))

### [1.3.1](https://gitlab.com/html-validate/renovate-config/compare/v1.3.0...v1.3.1) (2020-12-16)

### Bug Fixes

- disable dependency dashboard ([15dbac1](https://gitlab.com/html-validate/renovate-config/commit/15dbac1b561010ab156319b8ef3aa9df5ba9653a))

## [1.3.0](https://gitlab.com/html-validate/renovate-config/compare/v1.2.0...v1.3.0) (2020-12-13)

### Features

- enable `dependencyDashboard` ([087f8d7](https://gitlab.com/html-validate/renovate-config/commit/087f8d7e62c9daf2fc1cd9d56920b090dd8a7e59))

### Bug Fixes

- replace `unpublishSafe` with `stabilityDays` ([9d64807](https://gitlab.com/html-validate/renovate-config/commit/9d64807a6d1b7d056d2239ca54bbf7926ca09c63))

# [1.2.0](https://gitlab.com/html-validate/renovate-config/compare/v1.1.1...v1.2.0) (2020-11-01)

### Features

- new `proxy` preset ([f76fd98](https://gitlab.com/html-validate/renovate-config/commit/f76fd9829f11546dfd48e288e0447dec06053007))

## [1.1.1](https://gitlab.com/html-validate/renovate-config/compare/v1.1.0...v1.1.1) (2020-11-01)

### Bug Fixes

- add github token for release notes ([9c05d17](https://gitlab.com/html-validate/renovate-config/commit/9c05d17fba5566b4c0039fd7d6a13c92ddb7673b))
- refactor config to use predefined presets ([a8b1698](https://gitlab.com/html-validate/renovate-config/commit/a8b1698cf3e1e3c9e1ce011022792eda6e4c09a6))
- use `fix` when commiting any `dependency` or `peerDependency` ([3286ed3](https://gitlab.com/html-validate/renovate-config/commit/3286ed317f3b4c99302acdda86cfa6984417d855))

# [1.1.0](https://gitlab.com/html-validate/renovate-config/compare/v1.0.0...v1.1.0) (2020-01-21)

### Features

- enable rebaseStalePrs ([c32682a](https://gitlab.com/html-validate/renovate-config/commit/c32682a05d6affbbe1728ea38b5769aedaaac9f0))

# 1.0.0 (2019-12-24)

### Features

- initial release refactored from html-validate ([6899f0b](https://gitlab.com/html-validate/renovate-config/commit/6899f0b19aac93f79be8b31c5a4333cf187a938c))
