# @html-validate/renovate-config

Sharable config for Renovate

- Automerges patches and minor updates
- Raises pull requests for major or failed updates
- Semantic commits:
  - `devDependencies` -> `chore(deps)`
  - `dependencies` -> `fix(deps)`
  - `peerDependencies` -> `fix(deps)`

## Usage

Regular libraries can use `default` preset by adding this to their `package.json`:

```json
{
  "renovate": {
    "extends": ["gitlab>html-validate/renovate-config"]
  }
}
```

Bundled libraries/cli tools where all dependencies are included in the output bundle can use the `bundled` preset in addition to the `default` one:

```json
{
  "renovate": {
    "extends": [
      "gitlab>html-validate/renovate-config",
      "gitlab>html-validate/renovate-config:bundled"
    ]
  }
}
```

## Proxy packages (e.g. sharable configurations with dependencies)

For packages with sharable configurations there is a `proxy` preset which mimics the dependencies:

```json
{
  "renovate": {
    "extends": ["gitlab>html-validate/renovate-config:proxy"]
  }
}
```

Under this preset the semantic commits for non-devDependencies are:

- major -> `fix(deps)!`
- minor -> `fix(deps)`
- patch -> `fix(deps)`
